<!DOCTYPE html>
<html prefix="" lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width">
<title>Modular arithmetic | WifiCIDR Blog</title>
<link href="../../assets/css/all-nocdn.css" rel="stylesheet" type="text/css">
<link rel="alternate" type="application/rss+xml" title="RSS" href="../../rss.xml">
<link rel="canonical" href="https://blog.wificidr.net/posts/modular-arithmetic/">
<script type="text/x-mathjax-config">
MathJax.Hub.Config({
    tex2jax: {
        inlineMath: [ ['$','$'], ["\\(","\\)"] ],
        displayMath: [ ['$$','$$'], ["\\[","\\]"] ],
        processEscapes: true
    },
    displayAlign: 'center', // Change this to 'left' if you want left-aligned equations.
    "HTML-CSS": {
        styles: {'.MathJax_Display': {"margin": 0}}
    }
});
</script><!--[if lt IE 9]><script src="../../assets/js/html5.js"></script><![endif]--><meta name="author" content="Daniel Justice">
<meta property="og:site_name" content="WifiCIDR Blog">
<meta property="og:title" content="Modular arithmetic">
<meta property="og:url" content="https://blog.wificidr.net/posts/modular-arithmetic/">
<meta property="og:description" content="An introduction to modular arithmetic¶Most of my tech-savvy peers have heard of the RSA algorithm or RSA certificates.
It is commonly used to generate ssh key pairs, for example.
ssh-keygen -t rsa -f ">
<meta property="og:type" content="article">
<meta property="article:published_time" content="2023-07-31T18:42:56-05:00">
<meta property="article:tag" content="math">
<meta property="article:tag" content="rsa">
</head>
<body>
    

    <header id="header" class="navbar"><div class="container">
            
    <div class="brand">

        <div class="brand-text">
            <a href="https://blog.wificidr.net/" title="WifiCIDR Blog" rel="home">
                WifiCIDR Blog
            </a>
        </div>

        <a id="btn-toggle-nav" class="navbar-toggle">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </a>
    </div>

            
    <nav class="navbar-collapse collapse"><ul class="nav">
<li><a href="../../pages/running-results/">Running Results</a></li>
                <li><a href="../../archive.html">Archive</a></li>
                <li><a href="../../categories/">Tags</a></li>
                <li><a href="../../rss.xml">RSS feed</a></li>
    
    
    </ul></nav>
</div>
    </header><div class="header-padding"> </div>

    
    <div class="post-header">
        <div class="container">
            <div class="title">
                Modular arithmetic
            </div>
        </div>
    </div>

    <div class="post-meta">
      <div class="container">
	<div class="meta clearfix">
	  <div class="authordate">
	    <time class="timeago" datetime="2023-07-31T18:42:56-05:00">2023/07/31</time>
	    

	    
          |  
        <a href="index.ipynb" id="sourcelink">Source</a>

	  </div>
	  <div class="post-tags">
	    <div class="tag">
	      <a href="../../categories/math/" rel="tag">math</a>
	    </div>
	    <div class="tag">
	      <a href="../../categories/rsa/" rel="tag">rsa</a>
	    </div>
	  </div>
	</div>
      </div>
    </div>


    <div id="post-main" class="main">
        <div class="container">
        <div class="cell border-box-sizing text_cell rendered" id="cell-id=87a5ecbc">
<div class="prompt input_prompt">
</div>
<div class="inner_cell">
<div class="text_cell_render border-box-sizing rendered_html">
<h2 id="An-introduction-to-modular-arithmetic">An introduction to modular arithmetic<a class="anchor-link" href="#An-introduction-to-modular-arithmetic">¶</a>
</h2>
<p>Most of my tech-savvy peers have heard of the RSA algorithm or RSA certificates.
It is commonly used to generate ssh key pairs, for example.</p>
<div class="highlight"><pre><span></span>ssh-keygen<span class="w"> </span>-t<span class="w"> </span>rsa<span class="w"> </span>-f<span class="w"> </span>mykey
</pre></div>
<p>The command above produces two files, a private and public key.</p>
<div class="highlight"><pre><span></span>ll<span class="w"> </span>mykey*
.rw-------<span class="w"> </span><span class="m">2</span>.6k<span class="w"> </span>djustice<span class="w">  </span><span class="m">7</span><span class="w"> </span>Aug<span class="w"> </span><span class="m">18</span>:25<span class="w"> </span>mykey
.rw-r--r--<span class="w">  </span><span class="m">586</span><span class="w"> </span>djustice<span class="w">  </span><span class="m">7</span><span class="w"> </span>Aug<span class="w"> </span><span class="m">18</span>:25<span class="w"> </span>mykey.pub
</pre></div>
<p>You can share the public key, but the private key must be kept secure (also note the file permission differences).
Public means public, too!
You can see someone's public key on Github by appending <code>.keys</code> to their username.
It may respond with <code>ssh-rsa ...</code> or something else depending on the algorithm used to generate the key (EdDSA is common).</p>
<div class="highlight"><pre><span></span>curl<span class="w"> </span>https://github.com/some-user.keys
ssh-rsa<span class="w"> </span>lots-o-chars...
</pre></div>
<p>Many of us in the tech world have seen this base64-encoded soup daily, but have you ever peeked under the hood?
I think the RSA algorithm is one of the more tractable subjects in cryptography, and it opens the door to many other ideas in number theory.</p>
<p>The RSA algorithm works because of the properties of prime numbers and modular arithmetic.
I will start with the latter, and I hope to work the former into another post as I develop this topic.
My goal is to spark an interest, not to provide a rigorous discussion.
There is plenty of jargon to discuss, so please try to work through it.
There is a reason for many of these terms, and I will do my best to justify them as we go.</p>
<p>We will only be working with integers in this article, so think of the set of whole numbers from -∞ to ∞.
For example: {..., -3, -2, -1, 0, 1, 2, 3, ...}.
This set is known in mathematical circles as ℤ.
The symbol is used because it represents a precise idea in a compact space.</p>
<h3 id="Clock-arithmetic">Clock arithmetic<a class="anchor-link" href="#Clock-arithmetic">¶</a>
</h3>
<p>Some mathematicians don't like the clock metaphor for modular arithmetic, but I think it is a great starting point.
In my own experiences, I have had the most success explaining this subject to other people using clocks, and the comparison doesn't have any sharp edges that will confuse you later on.</p>
<p><img alt="wall clock" src="../../images/clock.jpg" title="wall clock"></p>
<p>Starting with a 12-hour wall clock, we will create a number system called "the integers modulus 12".
That is quite a bit to write several times in a row, so you will often see ℤ mod 12, or simply "mod 12".
This wouldn't be a techy post without some code!
The modulus function is essentially the remainder of division by some number; in the current case, 12.
Most programming languages perform the operation using the binary operator <code>%</code> (binary means it takes two arguments).</p>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered" id="cell-id=b0b9eeb1">
<div class="input">
<div class="prompt input_prompt">In [1]:</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3"><pre><span></span><span class="k">for</span> <span class="n">n</span> <span class="ow">in</span> <span class="nb">range</span><span class="p">(</span><span class="mi">1</span><span class="p">,</span> <span class="mi">14</span><span class="p">):</span>
    <span class="nb">print</span><span class="p">(</span><span class="s2">"The remainder of </span><span class="si">{n}</span><span class="s2"> divided by 12 is </span><span class="si">{r}</span><span class="s2">."</span><span class="o">.</span><span class="n">format</span><span class="p">(</span><span class="n">n</span><span class="o">=</span><span class="n">n</span><span class="p">,</span> <span class="n">r</span><span class="o">=</span><span class="n">n</span> <span class="o">%</span> <span class="mi">12</span><span class="p">))</span>
</pre></div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt"></div>
<div class="output_subarea output_stream output_stdout output_text">
<pre>The remainder of 1 divided by 12 is 1.
The remainder of 2 divided by 12 is 2.
The remainder of 3 divided by 12 is 3.
The remainder of 4 divided by 12 is 4.
The remainder of 5 divided by 12 is 5.
The remainder of 6 divided by 12 is 6.
The remainder of 7 divided by 12 is 7.
The remainder of 8 divided by 12 is 8.
The remainder of 9 divided by 12 is 9.
The remainder of 10 divided by 12 is 10.
The remainder of 11 divided by 12 is 11.
The remainder of 12 divided by 12 is 0.
The remainder of 13 divided by 12 is 1.
</pre>
</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing text_cell rendered" id="cell-id=933a1571">
<div class="prompt input_prompt">
</div>
<div class="inner_cell">
<div class="text_cell_render border-box-sizing rendered_html">
<p>Those first few lines can trip people up.
Why is 1 the remainder of 1 divided by 12?
It is because <code>12 * 0 + 1 = 1</code>.
Pay close attention to the last few values.
The remainders don't continue to increment without bound, they roll over back to zero!</p>
<p>Okay, I will admit that it probably isn't that exciting.
Most of us should remember these facts from grade school.
Continuing our introduction (or refresher), do you recall that we can perform arithmetic in this number system?
Ask yourself, if the hour hand is on 5 right now, what time will it be in 37 hours?
The answer is</p>
<pre><code>(5 + 37) mod 12 = 42 mod 12 = 6 o'clock

</code></pre>
<p>Here is another way to think of it.
<code>37 mod 12 = 1</code> and <code>5 + 1 = 6</code>.
Is that a coincidence?
It is not!</p>
<p>What about multiplication; does that work in our modular system?
What does <code>5 * 9</code> hours equal?</p>
<pre><code>(5 * 9) mod 12 = 45 mod 12 = 9 o'clock

</code></pre>
<p>This may seem a bit contrived, but there are practical applications.
It is 2 o'clock when you start your delivery run.
You drive 3 hours north to the warehouse, then make 3, 3-hour round-trips to a remote depot and back.
What time do you return to the warehouse?</p>
<pre><code>[(2 + 3) + (3 * 3)] mod 12 = (5 + 9) mod 12 = 14 mod 12 = 2 mod 12 = 2

</code></pre>
<p>This is fairly basic number theory; things you most likely already know.
If we dig a bit deeper, an interesting structure emerges.</p>
<p><img alt="mod12" src="../../images/mod12.png" title="mod 12"></p>
</div>
</div>
</div>
<div class="cell border-box-sizing text_cell rendered" id="cell-id=17f49147">
<div class="prompt input_prompt">
</div>
<div class="inner_cell">
<div class="text_cell_render border-box-sizing rendered_html">
<p>Consider the numbers on the ray originating at 4.</p>
<pre><code>{..., -20, -8, 4, 16, 28, 40, ...}

</code></pre>
<p>When performing arithmetic in <code>mod 12</code>, we can substitute any of these numbers with each other and achieve the same result.
Mathematicians call this an <em>equivalence class</em>.
The terminology is necessary because we are trying to describe a precise idea.
Clearly, -20 and 40 are <strong>not</strong> equal to one another.
However, in the <code>mod 12</code> number system, they are equivalent when we perform computations.
This is typically written as <code>[4]</code> where</p>
<pre><code>[4] = {..., -20, -8, 4, 16, 28, 40, ...}

</code></pre>
<p>Each number <code>n</code> in this set is <em>related</em> to ℤ by <code>n mod 12 = 4</code>, or more compactly: <code>{n | n ∈ ℤ, n mod 12 = 4}</code>.</p>
<p>Look at a few more of these equivalence classes:</p>
<pre><code>[0] = {..., -24, -12, 0, 12, 24, 36, ...},
[1] = {..., -23, -11, 1, 13, 25, 37, ...},
[2] = {..., -22, -10, 2, 14, 26, 38, ...},
...
[11] = {..., -13, -1, 11, 23, 35, 47, ...}

</code></pre>
<p>Start at any column and make your way down subtracting 1 each time.
When you get to the bottom, move one column to the right.
Notice anything interesting?
Every single number in ℤ is represented in one of these... partitions.
Division already has a definition in mathematics, so we will use the phrase <em>partition</em> to describe these collections of numbers.
The really neat part is that this system of partitions representing all the integers works in <em>any</em> modular base!
My teenage son who has no use for math even admitted that this is pretty cool, so put that in your pipe and smoke it!</p>
<h3 id="Homework">Homework<a class="anchor-link" href="#Homework">¶</a>
</h3>
<p>This wouldn't be an article on math without some homework.
Use your favorite search engine and read about the Caesar cipher.
Write an implementation in your favorite language and see if your friends can break it.
Just don't send it to me; I am terrible at cryptography.
😂
You can strengthen the cipher by using a random permutation of the alphabet, but both ends of the conversation must use the same permutation.
This is still susceptible to frequency attacks, so don't use it to send GPG keys over the internet.</p>
<h3 id="Next-steps">Next steps<a class="anchor-link" href="#Next-steps">¶</a>
</h3>
<p>This subject is inspired by the work I did on my research paper to earn my B.S. in Applied Mathematics.
My plan is to translate it in a way that will be consumable by most programmers (really anyone) who have a little bit of mathematics background.</p>
<p>If I don't get hit by a bus, I hope to write:</p>
<ul>
<li>
<a href="https://blog.wificidr.net/posts/prime-numbers-the-extended-euclidean-algorithm-and-the-gcd/">Prime numbers, the Extended Euclidean Algorithm, and the GCD</a>.</li>
<li>
<a href="https://blog.wificidr.net/posts/the-modular-inverse/">The Modular Inverse, an attempt to explain it without hand-waving</a>.</li>
<li>How RSA works (the math, not the code).</li>
</ul>
<p>I am going to provide a detailed guide, but it is up to the reader to sit down and draw their own conclusions about how these things work.
Math is not a spectator sport!</p>
</div>
</div>
</div>
<div class="cell border-box-sizing text_cell rendered" id="cell-id=d613bccc">
<div class="prompt input_prompt">
</div>
<div class="inner_cell">
<div class="text_cell_render border-box-sizing rendered_html">
<h3 id="References">References<a class="anchor-link" href="#References">¶</a>
</h3>
<p>Clock photo: <a href="https://commons.wikimedia.org/wiki/File:B_%26_HB_Kent_Pocket_Watch_(52584138758).jpg">https://commons.wikimedia.org/wiki/File:B_%26_HB_Kent_Pocket_Watch_(52584138758).jpg</a></p>
</div>
</div>
</div>
        
        
        <script src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.5/MathJax.js?config=TeX-AMS-MML_HTMLorMML" integrity="sha384-3lJUsx1TJHt7BA4udB5KPnDrlkO8T6J6v/op7ui0BbCjvZ9WqV4Xm6DTP6kQ/iBH" crossorigin="anonymous"></script><script type="text/x-mathjax-config">
MathJax.Hub.Config({
    tex2jax: {
        inlineMath: [ ['$','$'], ["\\(","\\)"] ],
        displayMath: [ ['$$','$$'], ["\\[","\\]"] ],
        processEscapes: true
    },
    displayAlign: 'center', // Change this to 'left' if you want left-aligned equations.
    "HTML-CSS": {
        styles: {'.MathJax_Display': {"margin": 0}}
    }
});
</script>
</div>
    </div>

    
    <footer><div class="container">
            <div class="social">
                <div class="social-entry">
                    <a href="mailto:djustice@wificidr.net" target="_blank">
                        <i class="fa fa-envelope-o"></i>
                    </a>
                </div>


                <div class="social-entry">
                    <a href="https://github.com/dgjustice" target="_blank">
                        <i class="fa fa-github"></i>
                    </a>
                </div>

                <div class="social-entry">
                    <a href="../../rss.xml" target="_blank">
                        <i class="fa fa-rss"></i> 
                    </a>
                </div>
            </div>
                <div class="copyright">
                    Contents © 2024         <a href="mailto:djustice@wificidr.net">Daniel Justice</a> - Powered by         <a href="https://getnikola.com" rel="nofollow">Nikola</a>         
<a rel="license" href="https://creativecommons.org/licenses/by-sa/4.0/">
  <img alt="Creative Commons License BY-SA" style="border-width:0; margin-bottom:12px;" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png"></a>

                    
                </div>
           
        </div>
    </footer><script src="../../assets/js/all-nocdn.js" type="text/javascript"></script>
</body>
</html>
