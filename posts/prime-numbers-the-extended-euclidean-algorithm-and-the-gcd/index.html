<!DOCTYPE html>
<html prefix="" lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width">
<title>Prime numbers, the Extended Euclidean Algorithm, and the GCD | WifiCIDR Blog</title>
<link href="../../assets/css/all-nocdn.css" rel="stylesheet" type="text/css">
<link rel="alternate" type="application/rss+xml" title="RSS" href="../../rss.xml">
<link rel="canonical" href="https://blog.wificidr.net/posts/prime-numbers-the-extended-euclidean-algorithm-and-the-gcd/">
<script type="text/x-mathjax-config">
MathJax.Hub.Config({
    tex2jax: {
        inlineMath: [ ['$','$'], ["\\(","\\)"] ],
        displayMath: [ ['$$','$$'], ["\\[","\\]"] ],
        processEscapes: true
    },
    displayAlign: 'center', // Change this to 'left' if you want left-aligned equations.
    "HTML-CSS": {
        styles: {'.MathJax_Display': {"margin": 0}}
    }
});
</script><!--[if lt IE 9]><script src="../../assets/js/html5.js"></script><![endif]--><meta name="author" content="Daniel Justice">
<meta property="og:site_name" content="WifiCIDR Blog">
<meta property="og:title" content="Prime numbers, the Extended Euclidean Algorithm, and the GCD">
<meta property="og:url" content="https://blog.wificidr.net/posts/prime-numbers-the-extended-euclidean-algorithm-and-the-gcd/">
<meta property="og:description" content="Prime Numbers, the Extended Euclidean Algorithm, and the GCD¶This is the second post in a short series on the RSA algorithm.
The first post was an overview of modular arithmetic, and this article will">
<meta property="og:type" content="article">
<meta property="article:published_time" content="2023-08-12T12:31:38-05:00">
<meta property="article:tag" content="math">
<meta property="article:tag" content="rsa">
</head>
<body>
    

    <header id="header" class="navbar"><div class="container">
            
    <div class="brand">

        <div class="brand-text">
            <a href="https://blog.wificidr.net/" title="WifiCIDR Blog" rel="home">
                WifiCIDR Blog
            </a>
        </div>

        <a id="btn-toggle-nav" class="navbar-toggle">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </a>
    </div>

            
    <nav class="navbar-collapse collapse"><ul class="nav">
<li><a href="../../pages/running-results/">Running Results</a></li>
                <li><a href="../../archive.html">Archive</a></li>
                <li><a href="../../categories/">Tags</a></li>
                <li><a href="../../rss.xml">RSS feed</a></li>
    
    
    </ul></nav>
</div>
    </header><div class="header-padding"> </div>

    
    <div class="post-header">
        <div class="container">
            <div class="title">
                Prime numbers, the Extended Euclidean Algorithm, and the GCD
            </div>
        </div>
    </div>

    <div class="post-meta">
      <div class="container">
	<div class="meta clearfix">
	  <div class="authordate">
	    <time class="timeago" datetime="2023-08-12T12:31:38-05:00">2023/08/12</time>
	    

	    
          |  
        <a href="index.ipynb" id="sourcelink">Source</a>

	  </div>
	  <div class="post-tags">
	    <div class="tag">
	      <a href="../../categories/math/" rel="tag">math</a>
	    </div>
	    <div class="tag">
	      <a href="../../categories/rsa/" rel="tag">rsa</a>
	    </div>
	  </div>
	</div>
      </div>
    </div>


    <div id="post-main" class="main">
        <div class="container">
        <div class="cell border-box-sizing text_cell rendered" id="cell-id=29ea443c">
<div class="prompt input_prompt">
</div>
<div class="inner_cell">
<div class="text_cell_render border-box-sizing rendered_html">
<h2 id="Prime-Numbers,-the-Extended-Euclidean-Algorithm,-and-the-GCD">Prime Numbers, the Extended Euclidean Algorithm, and the GCD<a class="anchor-link" href="#Prime-Numbers,-the-Extended-Euclidean-Algorithm,-and-the-GCD">¶</a>
</h2>
<p>This is the second post in a short series on the RSA algorithm.
The first post was an overview of modular arithmetic, and this article will attempt to cover a few more ideas needed before we dig into the weeds of the algorithm.
It may seem like I am jumping around a bit, but I think this highlights the brilliance of Rivest, Shamir, and Adleman.
They were able to compose an algorithm from pieces that may not seem immediately related to one another.</p>
<h3 id="A-note-on-studying">A note on studying<a class="anchor-link" href="#A-note-on-studying">¶</a>
</h3>
<p>I skipped some important points in the last article hoping to spark a few people's interest.
I will continue with the goal of keeping the writing light, but we do have to work with a few important theorems and definitions along the way.
Some of the best math advice I ever heard or read is that whenever you are faced with a Definition or a Theorem, stop what you are doing and write down some examples!
"The Math Sorcerer" has a great video about self-study on <a href="https://youtu.be/fb_v5Bc8PSk">YouTube</a>.</p>
<h3 id="More-review">More review<a class="anchor-link" href="#More-review">¶</a>
</h3>
<p>Formally speaking, the Division Algorithm is the basis of modular arithmetic.
This may feel pedantic, but the aim is to build our argument in small, clear steps.</p>
<h4 id="Definition:-Division-Algorithm">Definition: Division Algorithm<a class="anchor-link" href="#Definition:-Division-Algorithm">¶</a>
</h4>
<p>For all integers $a$ and $m$ with $m&gt;0$, there exist unique integers $q$ and $r$ such that
$$a=mq+r$$
where $0≤r&lt;m$ (Cummings 2022).</p>
<p>A bar or "pipe" is used to mean divides.
For example, if you see $3|6$, this means that 3 divides 6.
A bar with a slash across it means doesn't divide.
$3\nmid 7$.
One integer divides another only if the remainder is zero.</p>
<h4 id="Theorem:-Modular-arithmetic">Theorem: Modular arithmetic<a class="anchor-link" href="#Theorem:-Modular-arithmetic">¶</a>
</h4>
<p>For integers $a$,$r$, and $m$, it is said that $a$ is congruent to $r$ modulo $m$, and one writes $a \equiv r \pmod m$, if $m|(a−r)$ (Cummings 2022).</p>
<p>This concept should be familiar if you read my last article.
The new piece here is the idea of congruency.</p>
<h5 id="Equivalent,-but-not-equal">Equivalent, but not equal<a class="anchor-link" href="#Equivalent,-but-not-equal">¶</a>
</h5>
<p>You will see the word "congruent" in almost any article about the mathematical machinery of the RSA algorithm.
We all know that 4 is not equal to 16.
But what about our $\pmod{12}$ number system?
We can exchange them in equations and get the same result.
This is what it means to be congruent.
In a modular system, we say that 4 is congruent to 16 in $\pmod{12}$.
This is a good point to stop and write down some examples of your own!</p>
<h3 id="Prime-numbers">Prime numbers<a class="anchor-link" href="#Prime-numbers">¶</a>
</h3>
<p>I hope you aren't bored yet!
Number theory begins with many of the ideas you were taught about whole numbers as a child.
Prime numbers play a key role in RSA.
What does it mean to be prime?
Prime numbers are the "atoms" of the integers; they compose all other integers.
I have a bag of small wooden cubes that I have used when teaching my kids math.
A prime number is a number that can only be arranged in a line.
Composite numbers can be built as rectangles, squares or 3-dimensional combinations of squares and rectangles.</p>
<h5 id="Homework">Homework<a class="anchor-link" href="#Homework">¶</a>
</h5>
<p>I leave prime factorization and a formal definition of primes as an exercise for the reader.
If you are a programmer, write an implementation of the <a href="https://mathworld.wolfram.com/SieveofEratosthenes.html">Sieve of Eratosthenes</a> in your favorite language.
Want an old-school challenge?
Write it in <a href="https://n8ta.com/projects/awk_intermediate.html"><code>awk</code></a>.</p>
<h3 id="GCD">GCD<a class="anchor-link" href="#GCD">¶</a>
</h3>
<h4 id="Theorem:-Greatest-common-divisor">Theorem: Greatest common divisor<a class="anchor-link" href="#Theorem:-Greatest-common-divisor">¶</a>
</h4>
<p>Let $a$ and $b$ be integers. If $c|a$ and $c|b$ ,then $c$ is said to be a common divisor of $a$ and $b$.
The greatest common divisor of $a$ and $b$ is the largest integer $d$ such that $d|a$ and $d|b$.
This number is denoted $gcd(a,b)$  (Cummings 2022).</p>
<p>This should be more grade school maths.</p>
<ul>
<li>$\gcd(20, 24) = 4$</li>
<li>$\gcd(28, 35) = 7$</li>
</ul>
<h4 id="Co-prime-numbers">Co-prime numbers<a class="anchor-link" href="#Co-prime-numbers">¶</a>
</h4>
<p>Two numbers are called "co-prime" if they only have 1 as a common divisor.
In other words, their GCD = 1.
Some examples of co-prime pairs:</p>
<ul>
<li>7, 13</li>
<li>24, 71</li>
<li>112341234120042, 112341234120043</li>
</ul>
<p>Take a look at that last one and notice that they differ by 1.
Take a moment and convince yourself that any two integers with a difference of 1 are co-prime.
Are you convinced?</p>
<h5 id="Homework">Homework<a class="anchor-link" href="#Homework">¶</a>
</h5>
<p>Use this fact to explore one of the earliest proofs that there are an infinite number of primes!</p>
<h3 id="Euler's-totient-function">Euler's totient function<a class="anchor-link" href="#Euler's-totient-function">¶</a>
</h3>
<p><img alt="Euler" src="https://upload.wikimedia.org/wikipedia/commons/thumb/6/60/Leonhard_Euler_2.jpg/384px-Leonhard_Euler_2.jpg" title="Euler"></p>
<p>This guy shows up everywhere in math, doesn't he?!</p>
<p>Euler's totient function is also known as the Euler $\phi$-function.
Don't let the names spook you away; the concept is straightforward.</p>
<p>We are interested in how many numbers are co-prime with a number.
This calls for examples.
Think of the function as the length of a list.</p>
<ul>
<li>$\phi(7) = 6$ because 1, 2, 3, 4, 5, 6 are co-prime with 7; $\gcd(7, 1..6) = 1$.</li>
</ul>
<p>$\phi$ of any prime number is equal to $p - 1$ by the definition of a prime number (you looked that up, earlier, right?).</p>
<ul>
<li>$\phi(11) = 10$</li>
<li>$\phi(65537) = 65536$, AKA $(2^{16} + 1)$ and $2^{16}$ for you programmers.</li>
</ul>
<p>Composite numbers are a little trickier.
We have to perform a prime factorization and eliminate numbers with <strong>any</strong> common factors $\ne$ 1 (because 1 is a divisor of all integers).</p>
<ul>
<li>$\phi(24) = 8$ because 1, 5, 7, 11, 13, 17, 19, 23 are co-prime with 24.</li>
</ul>
<p>Again, 8 is the length of this list.</p>
<p>This topic will be new to many of you without much college-level math, even former engineering students.
It is not horribly complicated at first sight, but my brain exploded when I saw it used in other formulas...
Spoiler alert, we will see it used in another formula!</p>
<h4 id="Prime-factorization">Prime factorization<a class="anchor-link" href="#Prime-factorization">¶</a>
</h4>
<p>The strength of RSA depends solely on the ability to factor large numbers.
Quantum computers could theoretically provide a huge increase in our ability to factor these numbers, and efforts are underway by governments, academic institutions, and corporations to find replacement algorithms that are "post-quantum" safe.
If you want to know more about this, do a search for Peter Shor's algorithm.
The NIST (USA) has already chosen one key exchange suite called Kyber.
As of this writing, it has not been standardized, but companies such as <a href="https://security.googleblog.com/2023/08/toward-quantum-resilient-security-keys.html">Google</a> and <a href="https://cloudflare.net/news/news-details/2023/Cloudflare-Democratizes-Post-Quantum-Cryptography-By-Delivering-It-For-Free-By-Default/default.aspx">Cloudflare</a> are fast at work deploying it across their networks (articles linked in their names).
We don't have scalable quantum computers today, but there is a real threat that encrypted conversations recorded on the internet now could be decrypted in a decade or two.</p>
<h3 id="B%C3%A9zout's-identity">Bézout's identity<a class="anchor-link" href="#B%C3%A9zout's-identity">¶</a>
</h3>
<p><img alt="Étienne Bézout" src="https://mathshistory.st-andrews.ac.uk/Biographies/Bezout/Bezout_2.jpeg"></p>
<p>We need one more theorem before I wrap up with the Euclidean algorithm.
<strong>This theorem is very important to the inner machinery of RSA</strong>, so make sure you play with it and understand how it works.</p>
<h4 id="Theorem:-B%C3%A9zout's-identity">Theorem: Bézout's identity<a class="anchor-link" href="#Theorem:-B%C3%A9zout's-identity">¶</a>
</h4>
<p>If $a$ and $b$ are positive integers, then there exist integers $k$ and $l$ such that $\gcd(a,b) =ak+bl$ (Cummings 2022).</p>
<p>Note that $k$ and $l$ aren't necessarily positive.
Given $\gcd(28, 35) = 7$, how do we find $k$ and $l$?
In this case, simple inspection tells us that $k=-1$ and $l=1$, thus</p>
$$\gcd(28, 35)=(-1)(28) + (1)(35) = 7$$<p>Do you think $k$ and $l$ are unique?
Existence and uniqueness questions appear frequently in the study of mathematics.</p>
<p>7 and 3 are prime, and $\gcd(7, 3) = 1$.</p>
$$\gcd(7, 3) = 1 = (-2)(3) + (1)(7)$$<p>Before going much further, I want to introduce Cayley tables.</p>
</div>
</div>
</div>
<div class="cell border-box-sizing text_cell rendered" id="cell-id=5184351e">
<div class="prompt input_prompt">
</div>
<div class="inner_cell">
<div class="text_cell_render border-box-sizing rendered_html">
<h5 id="Cayley-tables">Cayley tables<a class="anchor-link" href="#Cayley-tables">¶</a>
</h5>
<p>Recall addition and multiplication tables from grade school.
We can do the same thing in modular arithmetic systems.
Using the $\pmod{3}$ system, here is the addition table:</p>
<div style="text-align: center"> $a + b \pmod{3}$ </div>
<pre><code>     | -7 | -6 | -5 | -4 | -3 | -2 | -1 |  0 |  1 |  2 |  3 |  4 |
+----+----+----+----+----+----+----+----+----+----+----+----+----+
  -7 |  1 |  2 |  0 |  1 |  2 |  0 |  1 |  2 |  0 |  1 |  2 |  0 |
  -6 |  2 |  0 |  1 |  2 |  0 |  1 |  2 |  0 |  1 |  2 |  0 |  1 |
  -5 |  0 |  1 |  2 |  0 |  1 |  2 |  0 |  1 |  2 |  0 |  1 |  2 |
  -4 |  1 |  2 |  0 |  1 |  2 |  0 |  1 |  2 |  0 |  1 |  2 |  0 |
  -3 |  2 |  0 |  1 |  2 |  0 |  1 |  2 |  0 |  1 |  2 |  0 |  1 |
  -2 |  0 |  1 |  2 |  0 |  1 |  2 |  0 |  1 |  2 |  0 |  1 |  2 |
  -1 |  1 |  2 |  0 |  1 |  2 |  0 |  1 |  2 |  0 |  1 |  2 |  0 |
   0 |  2 |  0 |  1 |  2 |  0 |  1 |  2 |  0 |  1 |  2 |  0 |  1 |
   1 |  0 |  1 |  2 |  0 |  1 |  2 |  0 |  1 |  2 |  0 |  1 |  2 |
   2 |  1 |  2 |  0 |  1 |  2 |  0 |  1 |  2 |  0 |  1 |  2 |  0 |
   3 |  2 |  0 |  1 |  2 |  0 |  1 |  2 |  0 |  1 |  2 |  0 |  1 |
   4 |  0 |  1 |  2 |  0 |  1 |  2 |  0 |  1 |  2 |  0 |  1 |  2 |

</code></pre>
<p>And here is the multiplication table:</p>
<pre><code>     | -7 | -6 | -5 | -4 | -3 | -2 | -1 |  0 |  1 |  2 |  3 |  4 |
+----+----+----+----+----+----+----+----+----+----+----+----+----+
  -7 |  1 |  0 |  2 |  1 |  0 |  2 |  1 |  0 |  2 |  1 |  0 |  2 |
  -6 |  0 |  0 |  0 |  0 |  0 |  0 |  0 |  0 |  0 |  0 |  0 |  0 |
  -5 |  2 |  0 |  1 |  2 |  0 |  1 |  2 |  0 |  1 |  2 |  0 |  1 |
  -4 |  1 |  0 |  2 |  1 |  0 |  2 |  1 |  0 |  2 |  1 |  0 |  2 |
  -3 |  0 |  0 |  0 |  0 |  0 |  0 |  0 |  0 |  0 |  0 |  0 |  0 |
  -2 |  2 |  0 |  1 |  2 |  0 |  1 |  2 |  0 |  1 |  2 |  0 |  1 |
  -1 |  1 |  0 |  2 |  1 |  0 |  2 |  1 |  0 |  2 |  1 |  0 |  2 |
   0 |  0 |  0 |  0 |  0 |  0 |  0 |  0 |  0 |  0 |  0 |  0 |  0 |
   1 |  2 |  0 |  1 |  2 |  0 |  1 |  2 |  0 |  1 |  2 |  0 |  1 |
   2 |  1 |  0 |  2 |  1 |  0 |  2 |  1 |  0 |  2 |  1 |  0 |  2 |
   3 |  0 |  0 |  0 |  0 |  0 |  0 |  0 |  0 |  0 |  0 |  0 |  0 |
   4 |  2 |  0 |  1 |  2 |  0 |  1 |  2 |  0 |  1 |  2 |  0 |  1 |

</code></pre>
<p>These can be quite useful in your study of the modular world, and they will make another appearance in the next aritcle.</p>
</div>
</div>
</div>
<div class="cell border-box-sizing text_cell rendered" id="cell-id=ec77023c">
<div class="prompt input_prompt">
</div>
<div class="inner_cell">
<div class="text_cell_render border-box-sizing rendered_html">
<h4 id="Back-to-B%C3%A9zout">Back to Bézout<a class="anchor-link" href="#Back-to-B%C3%A9zout">¶</a>
</h4>
<p>Do you notice any patterns in the previous tables?
It may not be immediately obvious that the tables are helpful, so I will provide an additional clue.
Consider a "clock" in $\pmod{3}$.</p>
<p><img alt="mod3" src="../../images/mod3.png"></p>
<p>Now take a tape with all the integers (positive and negative) written on it.
Center is at zero and wrap it around both ways.
Write this down on a piece of paper; do you see the result?
Pay special attention to where <em>multiples</em> of 7 land on the $\pmod{3}$ circle.</p>
<p>Again, Bézout tells us that there is a $k$ and $l$ that satisfies $\gcd(7, 3) =7k+3l$.
Multiples of 7 give us $k$, and corresponding multiple of 3 give us $l$.
I say corresponding because not every muliple of 3 will do.
Notice that it takes two "wraps" of our tape to hit a multiple of 7.</p>
<p><img alt="numline" src="../../images/numline.png"></p>
<p>Consider the sequence of differences between multiples of 7 and its nearest neighbor.
This results in the sequence</p>
$$[9−7, 15−14, 21−21, 30−28, 36−35, 42−42, 51−59, 57−56, . . .]$$<p>or</p>
$$[2, 1, 0, 2, 1, 0, 2, 1, . . .]$$<p>The pattern should be clear, and one knows it repeats because of the properties of modular arithmetic.</p>
<p>I hope this is starting to make a bit of sense now.
The symmetry of modular systems is fascinating to me, and I hope you have a better intuition of Bézout's identity.
This is a critical Theorem, and I feel like it gets overlooked in a lot of math books and articles.</p>
<h3 id="Extended-Euclidean-Algorithm">Extended Euclidean Algorithm<a class="anchor-link" href="#Extended-Euclidean-Algorithm">¶</a>
</h3>
<p>I am not going to spend much time on this point because the method well-documented in books and across the web.
The mechanics of it should start to feel familiar by this point.
If not, create some examples of your own and work a couple problems (you should do it regardless!).</p>
<p>The table here follows the procedure <a href="https://www.math.cmu.edu/~bkell/21110-2010s/extended-euclidean.html">described by Brian Kell</a>.</p>
<p>Beginning with $a=93060$ and $b=307$, use the division algorithm to find the values $q$ and $r$.
$a$ and $b$ were chosen such that $\gcd(a,b) =1$.
Back-substitute $a$ and $b$ appropriately, and iterate until the remainder is zero.</p>
<style type="text/css">
tr:nth-child(even) {
  background-color: #ECE1DE;
}
.thead { font-weight: bold; }
table, th, td {
  border: 1px solid black;
  border-collapse: collapse;
  padding: 10px;
}
</style>
<table class="tg">
<thead><tr>
<td class="thead">c</td>
<td class="thead">d</td>
<td class="thead">q</td>
<td class="thead">r</td>
<td class="thead">out</td>
</tr></thead>
<tbody>
<tr>
<td class="tg-0lax">93060</td>
<td class="tg-0lax">307</td>
<td class="tg-0lax">303</td>
<td class="tg-0lax">39</td>
<td class="tg-0lax">39 = 93060 - (303)(307)  = a - 303b</td>
</tr>
<tr>
<td class="tg-0lax">307</td>
<td class="tg-0lax">39</td>
<td class="tg-0lax">7</td>
<td class="tg-0lax">34</td>
<td class="tg-0lax">34= 307 - 397  = b - 7(a - 303b)  = -7a + 2122b</td>
</tr>
<tr>
<td class="tg-0lax">39</td>
<td class="tg-0lax">34</td>
<td class="tg-0lax">1</td>
<td class="tg-0lax">5</td>
<td class="tg-0lax">5 = 39 - 34  = (a - 303b) - (-7a + 2122b)  = 8a - 2425b</td>
</tr>
<tr>
<td class="tg-0lax">34</td>
<td class="tg-0lax">5</td>
<td class="tg-0lax">6</td>
<td class="tg-0lax">4</td>
<td class="tg-0lax">4 = 34 - 65  = (-7a + 2122b) - 6(8a - 2425b)</td>
</tr>
<tr>
<td class="tg-0lax">5</td>
<td class="tg-0lax">4</td>
<td class="tg-0lax">1</td>
<td class="tg-0lax">1</td>
<td class="tg-0lax">1 = 5 - 4  = (8a - 2425b) - (-55a + 161672b)  = 63a - 19097b</td>
</tr>
</tbody>
</table>
<p>The last row gives us the values of $k$ and $l$ that can be plugged into Bézout's identity.</p>
$$\gcd(93060, 307) = 1 = 63(93060) + -19097(307)$$<h3 id="Wrapping-up">Wrapping up<a class="anchor-link" href="#Wrapping-up">¶</a>
</h3>
<p>This article is quite a bit different than the <a href="https://blog.wificidr.net/posts/modular-arithmetic/">last one</a>, but we have to face the nitty-gritty details at some point.
I hope you are starting to see the connections between these three subjects even though you may be wondering where all this is going.
The next article will introduce the star of the show, the modular inverse!
Don't worry, there will be no surprises there if you can understand the ideas in this post.</p>
<h4 id="Resources">Resources<a class="anchor-link" href="#Resources">¶</a>
</h4>
<p>The web is full of great articles, but I stare at a screen all day.
If you are interested in basic number theory and an absolutely incredible introduction to proofs, I highly recommend "Proofs: A Long-Form Mathematics Textbook" by Jay Cummings.
For my technology friends, Kenneth Rosen's "Discrete mathematics and its applications" has earned itself a permanent spot on my desk.
It covers the number theory used here as well as many other topics relevant to programmers.</p>
</div>
</div>
</div>
<div class="cell border-box-sizing text_cell rendered" id="cell-id=e29b4ff0">
<div class="prompt input_prompt">
</div>
<div class="inner_cell">
<div class="text_cell_render border-box-sizing rendered_html">
<h3 id="References">References<a class="anchor-link" href="#References">¶</a>
</h3>
<ul>
<li>Cummings, J. (2021). Proofs: A Long-Form Mathematics Textbook.</li>
<li>Judson, T. (2021). Abstract algebra: Theory and Applications. Orthogonal Publishing L3c.</li>
<li>Kell, B. (2010). The extended euclidean algorithm. <a href="https://www.math.cmu.edu/~bkell/21110-2010s/extended-euclidean.html">https://www.math.cmu.edu/~bkell/21110-2010s/extended-euclidean.html</a>
</li>
<li>Rosen,  K.  (2011). Discrete mathematics and its applications.  McGraw-Hill Education.</li>
</ul>
<h3 id="Photos">Photos<a class="anchor-link" href="#Photos">¶</a>
</h3>
<ul>
<li>Euler portrait: <a href="https://commons.wikimedia.org/wiki/File:Leonhard_Euler_2.jpg">https://commons.wikimedia.org/wiki/File:Leonhard_Euler_2.jpg</a>
</li>
<li>Bézout portrait: <a href="https://mathshistory.st-andrews.ac.uk/Biographies/Bezout/">https://mathshistory.st-andrews.ac.uk/Biographies/Bezout/</a>
</li>
</ul>
</div>
</div>
</div>
        
        
        <script src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.5/MathJax.js?config=TeX-AMS-MML_HTMLorMML" integrity="sha384-3lJUsx1TJHt7BA4udB5KPnDrlkO8T6J6v/op7ui0BbCjvZ9WqV4Xm6DTP6kQ/iBH" crossorigin="anonymous"></script><script type="text/x-mathjax-config">
MathJax.Hub.Config({
    tex2jax: {
        inlineMath: [ ['$','$'], ["\\(","\\)"] ],
        displayMath: [ ['$$','$$'], ["\\[","\\]"] ],
        processEscapes: true
    },
    displayAlign: 'center', // Change this to 'left' if you want left-aligned equations.
    "HTML-CSS": {
        styles: {'.MathJax_Display': {"margin": 0}}
    }
});
</script>
</div>
    </div>

    
    <footer><div class="container">
            <div class="social">
                <div class="social-entry">
                    <a href="mailto:djustice@wificidr.net" target="_blank">
                        <i class="fa fa-envelope-o"></i>
                    </a>
                </div>


                <div class="social-entry">
                    <a href="https://github.com/dgjustice" target="_blank">
                        <i class="fa fa-github"></i>
                    </a>
                </div>

                <div class="social-entry">
                    <a href="../../rss.xml" target="_blank">
                        <i class="fa fa-rss"></i> 
                    </a>
                </div>
            </div>
                <div class="copyright">
                    Contents © 2024         <a href="mailto:djustice@wificidr.net">Daniel Justice</a> - Powered by         <a href="https://getnikola.com" rel="nofollow">Nikola</a>         
<a rel="license" href="https://creativecommons.org/licenses/by-sa/4.0/">
  <img alt="Creative Commons License BY-SA" style="border-width:0; margin-bottom:12px;" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png"></a>

                    
                </div>
           
        </div>
    </footer><script src="../../assets/js/all-nocdn.js" type="text/javascript"></script>
</body>
</html>
