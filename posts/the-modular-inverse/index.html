<!DOCTYPE html>
<html prefix="" lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width">
<title>The Modular Inverse | WifiCIDR Blog</title>
<link href="../../assets/css/all-nocdn.css" rel="stylesheet" type="text/css">
<link rel="alternate" type="application/rss+xml" title="RSS" href="../../rss.xml">
<link rel="canonical" href="https://blog.wificidr.net/posts/the-modular-inverse/">
<script type="text/x-mathjax-config">
MathJax.Hub.Config({
    tex2jax: {
        inlineMath: [ ['$','$'], ["\\(","\\)"] ],
        displayMath: [ ['$$','$$'], ["\\[","\\]"] ],
        processEscapes: true
    },
    displayAlign: 'center', // Change this to 'left' if you want left-aligned equations.
    "HTML-CSS": {
        styles: {'.MathJax_Display': {"margin": 0}}
    }
});
</script><!--[if lt IE 9]><script src="../../assets/js/html5.js"></script><![endif]--><meta name="author" content="Daniel Justice">
<meta property="og:site_name" content="WifiCIDR Blog">
<meta property="og:title" content="The Modular Inverse">
<meta property="og:url" content="https://blog.wificidr.net/posts/the-modular-inverse/">
<meta property="og:description" content="The Modular Inverse¶This is the third post in my short series on the RSA algorithm.
We met most of the cast of characters used in the algorithm in the last post.
Prime numbers, Euclid's algorithm, Béz">
<meta property="og:type" content="article">
<meta property="article:published_time" content="2023-09-20T14:46:01-05:00">
<meta property="article:tag" content="math">
<meta property="article:tag" content="rsa">
</head>
<body>
    

    <header id="header" class="navbar"><div class="container">
            
    <div class="brand">

        <div class="brand-text">
            <a href="https://blog.wificidr.net/" title="WifiCIDR Blog" rel="home">
                WifiCIDR Blog
            </a>
        </div>

        <a id="btn-toggle-nav" class="navbar-toggle">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </a>
    </div>

            
    <nav class="navbar-collapse collapse"><ul class="nav">
<li><a href="../../pages/running-results/">Running Results</a></li>
                <li><a href="../../archive.html">Archive</a></li>
                <li><a href="../../categories/">Tags</a></li>
                <li><a href="../../rss.xml">RSS feed</a></li>
    
    
    </ul></nav>
</div>
    </header><div class="header-padding"> </div>

    
    <div class="post-header">
        <div class="container">
            <div class="title">
                The Modular Inverse
            </div>
        </div>
    </div>

    <div class="post-meta">
      <div class="container">
	<div class="meta clearfix">
	  <div class="authordate">
	    <time class="timeago" datetime="2023-09-20T14:46:01-05:00">2023/09/20</time>
	    

	    
          |  
        <a href="index.ipynb" id="sourcelink">Source</a>

	  </div>
	  <div class="post-tags">
	    <div class="tag">
	      <a href="../../categories/math/" rel="tag">math</a>
	    </div>
	    <div class="tag">
	      <a href="../../categories/rsa/" rel="tag">rsa</a>
	    </div>
	  </div>
	</div>
      </div>
    </div>


    <div id="post-main" class="main">
        <div class="container">
        <div class="cell border-box-sizing text_cell rendered" id="cell-id=9b40912a">
<div class="prompt input_prompt">
</div>
<div class="inner_cell">
<div class="text_cell_render border-box-sizing rendered_html">
<h2 id="The-Modular-Inverse">The Modular Inverse<a class="anchor-link" href="#The-Modular-Inverse">¶</a>
</h2>
<p>This is the third post in my short series on the RSA algorithm.
We met most of the cast of characters used in the algorithm in the <a href="https://blog.wificidr.net/posts/prime-numbers-the-extended-euclidean-algorithm-and-the-gcd/">last post</a>.
Prime numbers, Euclid's algorithm, Bézout's identity, and of course, one of Euler's identities come together to form the foundation of our work.
The last detail is the modular inverse.
Conceptually, the idea is straightforward, but I feel many articles on RSA don't give it the attention it deserves.</p>
<h3 id="Identities">Identities<a class="anchor-link" href="#Identities">¶</a>
</h3>
<p>Many operations paired with sets have an element known as the identity element.
This should be a familiar idea even if you haven't seen the formal definition.</p>
<ul>
<li>0 is the identity of the set $\mathbb{R}$ and the operation of addition.  42 + 0 = 42</li>
<li>1 is the identity of the set $\mathbb{R}$ and the operation of multiplication.  42 * 1 = 42</li>
<li>[] is the identity of list concatenation.  [40, 41, 42] + [] = [40, 41, 42]</li>
</ul>
<p>Can you think of other examples?
What about the identity of a 3x3 matrix?</p>
$$\begin{bmatrix}
42 &amp; 99 &amp; 2\\
0 &amp; -35 &amp; 12\\
4 &amp; 2 &amp; 8
\end{bmatrix} \cdot
\begin{bmatrix}
1 &amp; 0 &amp; 0\\
0 &amp; 1 &amp; 0\\
0 &amp; 0 &amp; 1
\end{bmatrix} =
\begin{bmatrix}
42 &amp; 99 &amp; 2\\
0 &amp; -35 &amp; 12\\
4 &amp; 2 &amp; 8
\end{bmatrix}$$<h3 id="Inverses">Inverses<a class="anchor-link" href="#Inverses">¶</a>
</h3>
<p>Informally, and inverse simply "undoes" an operation.
Specifically, we are interested in an element that gives us back the identity.</p>
<ul>
<li>$42 + -42 = 0$</li>
<li>$42 \cdot \frac{1}{42} = 1$</li>
</ul>
<p>This works for matrices as well, but keep in mind that not all matrices have an inverse!</p>
$$\begin{bmatrix}
42 &amp; 99 &amp; 2\\
0 &amp; -35 &amp; 12\\
4 &amp; 2 &amp; 8
\end{bmatrix} \cdot
\begin{bmatrix}
\frac{38}{967} &amp; \frac{197}{1934} &amp; \frac{-629}{3868}\\
\frac{-6}{967} &amp; \frac{-41}{967} &amp; \frac{63}{967}\\
\frac{-35}{1934} &amp; \frac{-39}{967} &amp; \frac{735}{3868}
\end{bmatrix} =
\begin{bmatrix}
1 &amp; 0 &amp; 0\\
0 &amp; 1 &amp; 0\\
0 &amp; 0 &amp; 1
\end{bmatrix}$$<h4 id="Put-the-modular-in-the-inverse">Put the modular in the inverse<a class="anchor-link" href="#Put-the-modular-in-the-inverse">¶</a>
</h4>
<p>Consider a multiplication table for $\pmod 9$ (excluding 0).</p>
$$\begin{array}{c|c|c|c|c|c|c|c|c|}
     &amp; 1 &amp; 2 &amp; 3 &amp; 4 &amp; 5 &amp; 6 &amp; 7 &amp; 8 \\
\hline
    1 &amp; 1 &amp; 2 &amp; 3 &amp; 4 &amp; 5 &amp; 6 &amp; 7 &amp; 8 \\
\hline
    2 &amp; 2 &amp; 4 &amp; 6 &amp; 8 &amp; 1 &amp; 3 &amp; 5 &amp; 7 \\
\hline
    3 &amp; 3 &amp; 6 &amp; 0 &amp; 3 &amp; 6 &amp; 0 &amp; 3 &amp; 6 \\
\hline
    4 &amp; 4 &amp; 8 &amp; 3 &amp; 7 &amp; 2 &amp; 6 &amp; 1 &amp; 5 \\
\hline
    5 &amp; 5 &amp; 1 &amp; 6 &amp; 2 &amp; 7 &amp; 3 &amp; 8 &amp; 4 \\
\hline
    6 &amp; 6 &amp; 3 &amp; 0 &amp; 6 &amp; 3 &amp; 0 &amp; 6 &amp; 3 \\
\hline
    7 &amp; 7 &amp; 5 &amp; 3 &amp; 1 &amp; 8 &amp; 6 &amp; 4 &amp; 2 \\
\hline
    8 &amp; 8 &amp; 7 &amp; 6 &amp; 5 &amp; 4 &amp; 3 &amp; 2 &amp; 1 \\
\hline
\end{array}$$<p>A quick inspection will reveal that 1 is indeed the identity element for integers and the multiplication operation.
Our intuition is preserved! 😅
As I noted earlier, not all matrices have an inverse.
Likewise, not all integers have a modular inverse.
Inspect the table closely; can you figure out which numbers <strong>do not</strong> have an inverse and why?</p>
<p>The integers &lt; 9 that are coprime with 9 and their <em>modular inverse</em>:</p>
<ul>
<li>$8 * 8 \pmod{9} = 1$, 8 is the modular inverse of of 8 in $\pmod{9}$</li>
<li>$7 * 4 \pmod{9} = 1$, 4 is the modular inverse of of 7 in $\pmod{9}$</li>
<li>$5 * 2 \pmod{9} = 1$, 2 is the modular inverse of of 5 in $\pmod{9}$</li>
<li>$4 * 7 \pmod{9} = 1$, 7 is the modular inverse of of 4 in $\pmod{9}$</li>
<li>$2 * 5 \pmod{9} = 1$, 5 is the modular inverse of of 2 in $\pmod{9}$</li>
<li>$1 * 1 \pmod{9} = 1$, 1 is the modular inverse of of 1 in $\pmod{9}$</li>
</ul>
<h5 id="Seeking-intuition">Seeking intuition<a class="anchor-link" href="#Seeking-intuition">¶</a>
</h5>
<p>Given two integers $A$ and $B$, $A$ has a modular inverse in $\pmod{B}$ <em>if and only if A is coprime with B</em>.</p>
<p>Here is how I think about it.
Consider $4 \pmod{9}$.
You can start at <strong>any</strong> number and count by multiples of 4.
In $\pmod{9}$, you are guaranteed to hit 1 at some point!</p>
<ul>
<li>Start with 30</li>
<li>$30 \pmod{9} = 3$</li>
<li>count by 4's</li>
<li>$34 \pmod{9} = 7$</li>
<li>$38 \pmod{9} = 2$</li>
<li>$42 \pmod{9} = 6$</li>
<li>$46 \pmod{9} = 1$</li>
</ul>
<p>Can you think of a way to determine the number of steps needed?
I covered that in the last post, and it involves Euclid's algorithm and Bézout's identity.</p>
<h3 id="Revisiting-B%C3%A9zout-and-Euclid">Revisiting Bézout and Euclid<a class="anchor-link" href="#Revisiting-B%C3%A9zout-and-Euclid">¶</a>
</h3>
<p>Bézout's identity states:</p>
<ul>
<li>If $a$ and $b$ are positive integers, then there exist integers $k$ and $l$ such that $\gcd(a,b)=ak+bl$</li>
</ul>
<p>If $a$ and $b$ are chosen so that their $\gcd = 1$, then we want to find some integers $k$ and $l$ where $1=ak+bl$.
This is done using Euclid's algorithm as described in the <a href="https://blog.wificidr.net/posts/prime-numbers-the-extended-euclidean-algorithm-and-the-gcd/">previous post</a>.</p>
<h3 id="Conclusion">Conclusion<a class="anchor-link" href="#Conclusion">¶</a>
</h3>
<p>I hope this brief discussion clears some of the ideas around the modular inverse.
What we are trying to accomplish is given some value, scale it up in a modular space, then be able to get back to where we started.
Kind of sounds like an encryption scheme, doesn't it? 😁
In my next post in this series, I will finally describe the actual RSA algorithm and how all of these pieces fit together.</p>
</div>
</div>
</div>
        
        
        <script src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.5/MathJax.js?config=TeX-AMS-MML_HTMLorMML" integrity="sha384-3lJUsx1TJHt7BA4udB5KPnDrlkO8T6J6v/op7ui0BbCjvZ9WqV4Xm6DTP6kQ/iBH" crossorigin="anonymous"></script><script type="text/x-mathjax-config">
MathJax.Hub.Config({
    tex2jax: {
        inlineMath: [ ['$','$'], ["\\(","\\)"] ],
        displayMath: [ ['$$','$$'], ["\\[","\\]"] ],
        processEscapes: true
    },
    displayAlign: 'center', // Change this to 'left' if you want left-aligned equations.
    "HTML-CSS": {
        styles: {'.MathJax_Display': {"margin": 0}}
    }
});
</script>
</div>
    </div>

    
    <footer><div class="container">
            <div class="social">
                <div class="social-entry">
                    <a href="mailto:djustice@wificidr.net" target="_blank">
                        <i class="fa fa-envelope-o"></i>
                    </a>
                </div>


                <div class="social-entry">
                    <a href="https://github.com/dgjustice" target="_blank">
                        <i class="fa fa-github"></i>
                    </a>
                </div>

                <div class="social-entry">
                    <a href="../../rss.xml" target="_blank">
                        <i class="fa fa-rss"></i> 
                    </a>
                </div>
            </div>
                <div class="copyright">
                    Contents © 2024         <a href="mailto:djustice@wificidr.net">Daniel Justice</a> - Powered by         <a href="https://getnikola.com" rel="nofollow">Nikola</a>         
<a rel="license" href="https://creativecommons.org/licenses/by-sa/4.0/">
  <img alt="Creative Commons License BY-SA" style="border-width:0; margin-bottom:12px;" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png"></a>

                    
                </div>
           
        </div>
    </footer><script src="../../assets/js/all-nocdn.js" type="text/javascript"></script>
</body>
</html>
