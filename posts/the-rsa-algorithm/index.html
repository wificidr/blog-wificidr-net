<!DOCTYPE html>
<html prefix="" lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width">
<title>The RSA Algorithm | WifiCIDR Blog</title>
<link href="../../assets/css/all-nocdn.css" rel="stylesheet" type="text/css">
<link rel="alternate" type="application/rss+xml" title="RSS" href="../../rss.xml">
<link rel="canonical" href="https://blog.wificidr.net/posts/the-rsa-algorithm/">
<script type="text/x-mathjax-config">
MathJax.Hub.Config({
    tex2jax: {
        inlineMath: [ ['$','$'], ["\\(","\\)"] ],
        displayMath: [ ['$$','$$'], ["\\[","\\]"] ],
        processEscapes: true
    },
    displayAlign: 'center', // Change this to 'left' if you want left-aligned equations.
    "HTML-CSS": {
        styles: {'.MathJax_Display': {"margin": 0}}
    }
});
</script><!--[if lt IE 9]><script src="../../assets/js/html5.js"></script><![endif]--><meta name="author" content="Daniel Justice">
<meta property="og:site_name" content="WifiCIDR Blog">
<meta property="og:title" content="The RSA Algorithm">
<meta property="og:url" content="https://blog.wificidr.net/posts/the-rsa-algorithm/">
<meta property="og:description" content="The RSA Algorithm¶Ron Rivest, Adi Shamir and Leonard Adleman shared their encryption scheme with the world in 1977.
The algorithm that bears their name was independently discovered by the British GCHQ">
<meta property="og:type" content="article">
<meta property="article:published_time" content="2023-12-19T18:26:05-06:00">
<meta property="article:tag" content="math">
<meta property="article:tag" content="rsa">
</head>
<body>
    

    <header id="header" class="navbar"><div class="container">
            
    <div class="brand">

        <div class="brand-text">
            <a href="https://blog.wificidr.net/" title="WifiCIDR Blog" rel="home">
                WifiCIDR Blog
            </a>
        </div>

        <a id="btn-toggle-nav" class="navbar-toggle">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </a>
    </div>

            
    <nav class="navbar-collapse collapse"><ul class="nav">
<li><a href="../../pages/running-results/">Running Results</a></li>
                <li><a href="../../archive.html">Archive</a></li>
                <li><a href="../../categories/">Tags</a></li>
                <li><a href="../../rss.xml">RSS feed</a></li>
    
    
    </ul></nav>
</div>
    </header><div class="header-padding"> </div>

    
    <div class="post-header">
        <div class="container">
            <div class="title">
                The RSA Algorithm
            </div>
        </div>
    </div>

    <div class="post-meta">
      <div class="container">
	<div class="meta clearfix">
	  <div class="authordate">
	    <time class="timeago" datetime="2023-12-19T18:26:05-06:00">2023/12/19</time>
	    

	    
          |  
        <a href="index.ipynb" id="sourcelink">Source</a>

	  </div>
	  <div class="post-tags">
	    <div class="tag">
	      <a href="../../categories/math/" rel="tag">math</a>
	    </div>
	    <div class="tag">
	      <a href="../../categories/rsa/" rel="tag">rsa</a>
	    </div>
	  </div>
	</div>
      </div>
    </div>


    <div id="post-main" class="main">
        <div class="container">
        <div class="cell border-box-sizing text_cell rendered" id="cell-id=f2bda544">
<div class="prompt input_prompt">
</div>
<div class="inner_cell">
<div class="text_cell_render border-box-sizing rendered_html">
<h2 id="The-RSA-Algorithm">The RSA Algorithm<a class="anchor-link" href="#The-RSA-Algorithm">¶</a>
</h2>
<p>Ron Rivest, Adi Shamir and Leonard Adleman shared their encryption scheme with the world in 1977.
The algorithm that bears their name was independently discovered by the British GCHQ a few years ealier, but they deemed it too sensitive to share with the world.
This is another great example of <a href="https://www.math.fsu.edu/~wxm/Arnold.htm">Arnold's Principle</a>.
My goal is to finally work through an example of the full algorithm.
Please see my previous three posts on the subject if you need more details on the individual steps.
I am not picking on this article in particular; it just happens to be the most recent one I have read.
Most articles on the topic involve quite a bit of hand waving as in 
<a href="https://pagedout.institute/download/PagedOut_003_beta1.p">Beyond The 
Illusion -Breaking RSA Encryption</a> by 
<a href="https://www.divd.nl/people/Max%20van%20der%20Horst/">Max Van Der Horst</a>.
They are absolutely correct in asserting that you should not roll your own cryptography, but I hope to explain the steps with a bit more clarity.
At a high level, RSA isn't the most complicated algorithm in the world, but breaking it is non-trivial.
It would not be so ubiquitous otherwise.</p>
</div>
</div>
</div>
<div class="cell border-box-sizing text_cell rendered" id="cell-id=415fdb4e-00d7-4d5c-b148-ff0d692c96d2">
<div class="prompt input_prompt">
</div>
<div class="inner_cell">
<div class="text_cell_render border-box-sizing rendered_html">
<h3 id="Key-selection">Key selection<a class="anchor-link" href="#Key-selection">¶</a>
</h3>
<ol>
<li>The receiver chooses two prime numbers $p$ and $q$ and calculates their product $N$.</li>
<li>Compute $\phi(N)$. Since $N$ is the product of two primes, $\phi(N) = (p − 1)(q − 1)$.</li>
<li>Choose a number $e$ that is relatively prime to both $N$ and $\phi(N)$. 65,537 is frequently used due to its <a href="https://brilliant.org/wiki/rsa-encryption/">desirable binary properties</a>.</li>
<li>$(e, N)$ is the public key.</li>
<li>Calculate the <a href="https://blog.wificidr.net/posts/the-modular-inverse/">modular inverse</a> $d$ of $e$ $(\pmod \phi(N))$. $(d, N)$ is the private key.</li>
</ol>
<h4 id="Key-selection---example">Key selection - example<a class="anchor-link" href="#Key-selection---example">¶</a>
</h4>
<ol>
<li>Choose $p = 331$ and $q = 283$. Compute $N = 331 · 283 = 93673$.</li>
<li>Compute $\phi(N) = (p − 1)(q − 1) = 330 \cdot 282 = 93060$.</li>
<li>Choose $e = 307$.</li>
<li>$(307, 93673)$ is the public key.</li>
<li>Calculate the modular inverse $d$ of $307 (\pmod \phi(93673)) = 307 (\pmod 93060) = 73963$.</li>
</ol>
<p>This step is demonstrated using the <a href="https://blog.wificidr.net/posts/prime-numbers-the-extended-euclidean-algorithm-and-the-gcd/">Extended Euclidean Algorithm</a> that I wrote about previously.
$(73963, 93673)$ is the private key where the first value is the modular inverse d and the second is the product N computed in step 1.</p>
<h3 id="Message-Encryption-and-Decryption">Message Encryption and Decryption<a class="anchor-link" href="#Message-Encryption-and-Decryption">¶</a>
</h3>
<p>Let's encrypt "ET TU, BRUTE".
As a side note, real-world implmentations encrypt/decrypt blocks of bytes, not a character at a time.
Begin by converting the letters to numbers using a simple map where A=0, B=1, ..., Z=25.
This results in $[4, 19, 26, 19, 20, 27, 26, 1, 17, 20, 19, 4]$.
Using the public key $(307, 93673)$, compute $c^{307} (\pmod 93673)$ for each character.
These numbers can be quite large, so be mindful of the limitations of the software you are using as many programming languages fail to warn of overflow errors.
Python has native support for large integers which makes this calculation easy to perform without precision issues.</p>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered" id="cell-id=b80c7681-48d5-4509-9326-d2252a461e93">
<div class="input">
<div class="prompt input_prompt">In [1]:</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3"><pre><span></span><span class="n">et_tu</span> <span class="o">=</span> <span class="p">[</span><span class="mi">4</span> <span class="p">,</span> <span class="mi">19</span> <span class="p">,</span> <span class="mi">26</span> <span class="p">,</span> <span class="mi">19</span> <span class="p">,</span> <span class="mi">20</span> <span class="p">,</span> <span class="mi">27</span> <span class="p">,</span> <span class="mi">26</span> <span class="p">,</span> <span class="mi">1</span> <span class="p">,</span> <span class="mi">17</span> <span class="p">,</span> <span class="mi">20</span> <span class="p">,</span> <span class="mi">19</span> <span class="p">,</span> <span class="mi">4</span><span class="p">]</span>
<span class="n">ciph</span> <span class="o">=</span> <span class="nb">list</span> <span class="p">(</span><span class="nb">map</span> <span class="p">(</span><span class="k">lambda</span> <span class="n">n</span><span class="p">:</span> <span class="nb">pow</span> <span class="p">(</span><span class="n">n</span> <span class="p">,</span> <span class="mi">307</span> <span class="p">,</span> <span class="mi">93673</span><span class="p">)</span> <span class="p">,</span> <span class="n">et_tu</span> <span class="p">))</span>
<span class="nb">print</span><span class="p">(</span><span class="n">ciph</span><span class="p">)</span>
</pre></div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt"></div>
<div class="output_subarea output_stream output_stdout output_text">
<pre>[43857, 26421, 21480, 26421, 44114, 11186, 21480, 1, 54440, 44114, 26421, 43857]
</pre>
</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing text_cell rendered" id="cell-id=67ae7b71-5114-499c-921f-a7e65f4e47e2">
<div class="prompt input_prompt">
</div>
<div class="inner_cell">
<div class="text_cell_render border-box-sizing rendered_html">
<p>These are the integers that someone would send over an insecure medium to the receiver.
On the other end, the receiver uses the private key $(73963, 93673)$ to compute $i^{73963} (\pmod 93673)$ for each integer received.</p>
</div>
</div>
</div>
<div class="cell border-box-sizing code_cell rendered" id="cell-id=8717122e-dbd1-4b8c-b454-6d0aac627b3e">
<div class="input">
<div class="prompt input_prompt">In [2]:</div>
<div class="inner_cell">
<div class="input_area">
<div class="highlight hl-ipython3"><pre><span></span><span class="n">msg</span> <span class="o">=</span> <span class="nb">list</span> <span class="p">(</span><span class="nb">map</span><span class="p">(</span> <span class="k">lambda</span> <span class="n">n</span><span class="p">:</span> <span class="nb">pow</span> <span class="p">(</span><span class="n">n</span> <span class="p">,</span> <span class="mi">73963</span> <span class="p">)</span> <span class="o">%</span> <span class="mi">93673</span> <span class="p">,</span> <span class="n">ciph</span><span class="p">))</span>
<span class="nb">print</span><span class="p">(</span><span class="n">msg</span><span class="p">)</span>
</pre></div>
</div>
</div>
</div>
<div class="output_wrapper">
<div class="output">
<div class="output_area">
<div class="prompt"></div>
<div class="output_subarea output_stream output_stdout output_text">
<pre>[4, 19, 26, 19, 20, 27, 26, 1, 17, 20, 19, 4]
</pre>
</div>
</div>
</div>
</div>
</div>
<div class="cell border-box-sizing text_cell rendered" id="cell-id=5796b5b8-00a5-4645-be9e-4675f684f78f">
<div class="prompt input_prompt">
</div>
<div class="inner_cell">
<div class="text_cell_render border-box-sizing rendered_html">
<p>A simple review quickly validates that this is indeed our original message!
It is worth highlighting once more that the private key is never shared.
The RSA algorithm is not paricularly fast, especially for handheld devices like cellular phones.
It is typically used to exchange a <a href="https://www.cloudflare.com/learning/ssl/what-is-a-session-key/">randomly generated key</a> that is unique to each HTTPS session between a client and server.
The remainder of the data can be sent using much faster symmetric encryption schemes using session keys.</p>
<h3 id="Intermezzo---Multiplying-Large-Integers">Intermezzo - Multiplying Large Integers<a class="anchor-link" href="#Intermezzo---Multiplying-Large-Integers">¶</a>
</h3>
<p>The encryption and decryption steps of the RSA Algorithm require computing the products of very large integers.
93,673 only requires 17 bits of storage, or less than 3 bytes.
What about a number such as $26^{93673}$?
The Python programming language will gladly attempt to compute it and succeeds on a modern laptop.</p>
<div class="highlight"><pre><span></span><span class="o">&gt;&gt;&gt;</span> <span class="nb">pow</span><span class="p">(</span><span class="mi">26</span><span class="p">,</span> <span class="mi">93673</span><span class="p">)</span><span class="o">.</span><span class="n">bit_length</span><span class="p">()</span>
<span class="mi">440305</span>
</pre></div>
<p>That is a massive number over 134,000 digits long!
Thankfully, this particular result is not needed, but the result modulus another number.
For example, using a number easily computed by hand, what is $26^{307}\pmod{47}$?
Start by converting the exponent to binary.
307 in base-2 is equal to 100110011.
Exponentiation works in $\mathbb{Z}_n$ because of the properties listed earlier.
An exponent is simply repeated multiplication.
If $b \equiv a^x \pmod{n}$ and $c\equiv a^y\pmod{n}$, then $bc \equiv a^{x+y}\pmod{n}$.
$$
\begin{aligned}
    26^{307} &amp;= 26^{2^0+2^1+2^4+2^5+2^8} \\
    		 &amp;\equiv 307^{2^0}+307^{2^1}+307^{2^4}+307^{2^5}+307^{2^8}\pmod{47} \\
\end{aligned}
$$
Calculate $2^i$ where $i=0, 1, 4, 5, 8$.
$$26^{2^1}=676\equiv18 \pmod{47}$$
This can be squared to find $26^{2^2}\pmod{47}$.
$$\begin{aligned}
    26^{2^2}&amp;\equiv (26^{2^1})^2\pmod{47} \\
    		&amp;\equiv (18)^2\pmod{47} \\
    		&amp;\equiv 324\pmod{47} \\
    		&amp;\equiv 42\pmod{47} \\
\end{aligned}$$
Use the fact that $(a^{2^n})^2\equiv a^{2\cdot2^n}\equiv a^{2^{n+1}}\pmod{n}$ \parencite{abstract}.
$$\begin{aligned}
    26^{2^4}&amp;\equiv 14\pmod{47} \\
    26^{2^5}&amp;\equiv 8\pmod{47} \\
    26^{2^8}&amp;\equiv 2\pmod{47} \\
    26^{307}&amp;\equiv 26*18*14*8*2 \pmod{47}\\
    &amp;\equiv 104832 \pmod{47}\\
    &amp;\equiv 22 \pmod{47}\\
\end{aligned}$$</p>
<p>Other than speed, this method saves memory as well since the largest number the computer must store is $2\log_2{exp}$ bits.
In general, an $n$-bit number multiplied by an $n$-bit number is $2n$-bits long.
There are two approaches known as left-to-right (LR) and right-to-left (RL) depending on the direction that the bits of the exponent are scanned.
The method above uses the RL method, but <a href="https://cetinkayakoc.net/docs/r02.pdf">LR is more common in hardware implementations</a> of the calculation.</p>
<h3 id="Euler%E2%80%99s-theorem">Euler’s theorem<a class="anchor-link" href="#Euler%E2%80%99s-theorem">¶</a>
</h3>
<p>The RSA algorithm works because of Euler’s theorem.
This is consistently the most hand-wavy aspect of most articles I read on this subject.
It appears magical, but it's a beautiful result in mathematics if you dig into it a bit!</p>
<h4 id="Theorem">Theorem<a class="anchor-link" href="#Theorem">¶</a>
</h4>
<p>Let $a$ and $n$ be integers such that $n &gt; 0$ and $gcd(a, n) = 1$.
Then $a^{\phi(n)} \equiv 1 (\pmod n)$.</p>
<p>Recall that step 5 of the key selection requires finding</p>
$$td\equiv 1\pmod{\phi(N)}.$$<p>Using the definition of <a href="https://blog.wificidr.net/posts/prime-numbers-the-extended-euclidean-algorithm-and-the-gcd/">modular congruence</a>,</p>
$$td = 1 + k\cdot \phi(N)$$<p>for some integer $k$.
The message is encrypted by finding $a^t \pmod{N}$ and decrypted by raising the encrypted message to the power of $d\pmod{N}$.</p>
$$(a^t)^d \pmod{N}$$<p>This gives back $a$, the original message.</p>
$$\begin{aligned}
    (a^t)^d &amp;= a^{td} \\
    		&amp;= a^{1+k\cdot\phi(N)} \\
    		&amp;= a\cdot \left( a^{\phi(N)} \right)^k \\
    		&amp;\equiv a\cdot1^k \\
    		&amp;\equiv a\pmod{N} \\
\end{aligned}$$<p>Euler's theorem may appear a bit magical at first.
Consider the Caley table of integers $\pmod{9}$.</p>
</div>
</div>
</div>
<div class="cell border-box-sizing text_cell rendered" id="cell-id=e550e20c-ca6c-4dca-8f11-fac29a5b184e">
<div class="prompt input_prompt">
</div>
<div class="inner_cell">
<div class="text_cell_render border-box-sizing rendered_html">
<p><img alt="Caley table" src="../../images/caley.png"></p>
<p>Take note of the entries with a value of 1.
These only occur when a number is coprime with 9.
The number of 1 entries in the Caley table is the same value as $\phi(n)$.
For example, 11 is coprime with 9.</p>
$$\begin{aligned}
    2 \equiv 11 \pmod{9} \\
    11^2 \equiv 4 \pmod{9} \\
    11^3 \equiv 8 \pmod{9} \\
    11^4 \equiv 7 \pmod{9} \\
    11^5 \equiv 5 \pmod{9} \\
    11^6 \equiv 1 \pmod{9} \\
\end{aligned}$$<p>This process works the same even if you start with a value of 2.
Increasing the power will always work towards a value of 1 as long as the GCD of $a$ and $n$ is 1.</p>
</div>
</div>
</div>
<div class="cell border-box-sizing text_cell rendered" id="cell-id=88fb48e4-a454-4477-9b18-154aaae2faa0">
<div class="prompt input_prompt">
</div>
<div class="inner_cell">
<div class="text_cell_render border-box-sizing rendered_html">
<h3 id="Conclusion">Conclusion<a class="anchor-link" href="#Conclusion">¶</a>
</h3>
<p>I hope my previous writing on this subject coupled with this explanation will give you a better idea of how RSA works under the hood.
I think it rests on a beautiful set of results in mathematics, and the genius of the original creators is how they crafted these ideas into a useful protocol.
Quantum computing has already made an impact on cryptography, but I trust this algorithm will be around for many more years as we transition to new, post-quantum technologies.</p>
</div>
</div>
</div>
        
        
        <script src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.5/MathJax.js?config=TeX-AMS-MML_HTMLorMML" integrity="sha384-3lJUsx1TJHt7BA4udB5KPnDrlkO8T6J6v/op7ui0BbCjvZ9WqV4Xm6DTP6kQ/iBH" crossorigin="anonymous"></script><script type="text/x-mathjax-config">
MathJax.Hub.Config({
    tex2jax: {
        inlineMath: [ ['$','$'], ["\\(","\\)"] ],
        displayMath: [ ['$$','$$'], ["\\[","\\]"] ],
        processEscapes: true
    },
    displayAlign: 'center', // Change this to 'left' if you want left-aligned equations.
    "HTML-CSS": {
        styles: {'.MathJax_Display': {"margin": 0}}
    }
});
</script>
</div>
    </div>

    
    <footer><div class="container">
            <div class="social">
                <div class="social-entry">
                    <a href="mailto:djustice@wificidr.net" target="_blank">
                        <i class="fa fa-envelope-o"></i>
                    </a>
                </div>


                <div class="social-entry">
                    <a href="https://github.com/dgjustice" target="_blank">
                        <i class="fa fa-github"></i>
                    </a>
                </div>

                <div class="social-entry">
                    <a href="../../rss.xml" target="_blank">
                        <i class="fa fa-rss"></i> 
                    </a>
                </div>
            </div>
                <div class="copyright">
                    Contents © 2024         <a href="mailto:djustice@wificidr.net">Daniel Justice</a> - Powered by         <a href="https://getnikola.com" rel="nofollow">Nikola</a>         
<a rel="license" href="https://creativecommons.org/licenses/by-sa/4.0/">
  <img alt="Creative Commons License BY-SA" style="border-width:0; margin-bottom:12px;" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png"></a>

                    
                </div>
           
        </div>
    </footer><script src="../../assets/js/all-nocdn.js" type="text/javascript"></script>
</body>
</html>
