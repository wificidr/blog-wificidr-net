{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "f2bda544",
   "metadata": {},
   "source": [
    "# The RSA Algorithm\n",
    "\n",
    "Ron Rivest, Adi Shamir and Leonard Adleman shared their encryption scheme with the world in 1977.\n",
    "The algorithm that bears their name was independently discovered by the British GCHQ a few years ealier, but they deemed it too sensitive to share with the world.\n",
    "This is another great example of [Arnold's Principle](https://www.math.fsu.edu/~wxm/Arnold.htm).\n",
    "My goal is to finally work through an example of the full algorithm.\n",
    "Please see my previous three posts on the subject if you need more details on the individual steps.\n",
    "I am not picking on this article in particular; it just happens to be the most recent one I have read.\n",
    "Most articles on the topic involve quite a bit of hand waving as in \n",
    "[Beyond The \n",
    "Illusion -Breaking RSA Encryption](https://pagedout.institute/download/PagedOut_003_beta1.p) by \n",
    "[Max Van Der Horst](https://www.divd.nl/people/Max%20van%20der%20Horst/).\n",
    "They are absolutely correct in asserting that you should not roll your own cryptography, but I hope to explain the steps with a bit more clarity.\n",
    "At a high level, RSA isn't the most complicated algorithm in the world, but breaking it is non-trivial.\n",
    "It would not be so ubiquitous otherwise."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "415fdb4e-00d7-4d5c-b148-ff0d692c96d2",
   "metadata": {},
   "source": [
    "## Key selection\n",
    "\n",
    "1. The receiver chooses two prime numbers $p$ and $q$ and calculates their product $N$.\n",
    "2. Compute $\\phi(N)$. Since $N$ is the product of two primes, $\\phi(N) = (p − 1)(q − 1)$.\n",
    "3. Choose a number $e$ that is relatively prime to both $N$ and $\\phi(N)$. 65,537 is frequently used due to its [desirable binary properties](https://brilliant.org/wiki/rsa-encryption/).\n",
    "4. $(e, N)$ is the public key.\n",
    "5. Calculate the [modular inverse](https://blog.wificidr.net/posts/the-modular-inverse/) $d$ of $e$ $(\\pmod \\phi(N))$. $(d, N)$ is the private key.\n",
    "\n",
    "### Key selection - example\n",
    "\n",
    "1. Choose $p = 331$ and $q = 283$. Compute $N = 331 · 283 = 93673$.\n",
    "2. Compute $\\phi(N) = (p − 1)(q − 1) = 330 \\cdot 282 = 93060$.\n",
    "3. Choose $e = 307$.\n",
    "4. $(307, 93673)$ is the public key.\n",
    "5. Calculate the modular inverse $d$ of $307 (\\pmod \\phi(93673)) = 307 (\\pmod 93060) = 73963$.\n",
    "\n",
    "This step is demonstrated using the [Extended Euclidean Algorithm](https://blog.wificidr.net/posts/prime-numbers-the-extended-euclidean-algorithm-and-the-gcd/) that I wrote about previously.\n",
    "$(73963, 93673)$ is the private key where the first value is the modular inverse d and the second is the product N computed in step 1.\n",
    "\n",
    "## Message Encryption and Decryption\n",
    "\n",
    "Let's encrypt \"ET TU, BRUTE\".\n",
    "As a side note, real-world implmentations encrypt/decrypt blocks of bytes, not a character at a time.\n",
    "Begin by converting the letters to numbers using a simple map where A=0, B=1, ..., Z=25.\n",
    "This results in $[4, 19, 26, 19, 20, 27, 26, 1, 17, 20, 19, 4]$.\n",
    "Using the public key $(307, 93673)$, compute $c^{307} (\\pmod 93673)$ for each character.\n",
    "These numbers can be quite large, so be mindful of the limitations of the software you are using as many programming languages fail to warn of overflow errors.\n",
    "Python has native support for large integers which makes this calculation easy to perform without precision issues."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "b80c7681-48d5-4509-9326-d2252a461e93",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[43857, 26421, 21480, 26421, 44114, 11186, 21480, 1, 54440, 44114, 26421, 43857]\n"
     ]
    }
   ],
   "source": [
    "et_tu = [4 , 19 , 26 , 19 , 20 , 27 , 26 , 1 , 17 , 20 , 19 , 4]\n",
    "ciph = list (map (lambda n: pow (n , 307 , 93673) , et_tu ))\n",
    "print(ciph)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "67ae7b71-5114-499c-921f-a7e65f4e47e2",
   "metadata": {},
   "source": [
    "These are the integers that someone would send over an insecure medium to the receiver.\n",
    "On the other end, the receiver uses the private key $(73963, 93673)$ to compute $i^{73963} (\\pmod 93673)$ for each integer received."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "8717122e-dbd1-4b8c-b454-6d0aac627b3e",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[4, 19, 26, 19, 20, 27, 26, 1, 17, 20, 19, 4]\n"
     ]
    }
   ],
   "source": [
    "msg = list (map( lambda n: pow (n , 73963 ) % 93673 , ciph))\n",
    "print(msg)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5796b5b8-00a5-4645-be9e-4675f684f78f",
   "metadata": {},
   "source": [
    "A simple review quickly validates that this is indeed our original message!\n",
    "It is worth highlighting once more that the private key is never shared.\n",
    "The RSA algorithm is not paricularly fast, especially for handheld devices like cellular phones.\n",
    "It is typically used to exchange a [randomly generated key](https://www.cloudflare.com/learning/ssl/what-is-a-session-key/) that is unique to each HTTPS session between a client and server.\n",
    "The remainder of the data can be sent using much faster symmetric encryption schemes using session keys.\n",
    "\n",
    "## Intermezzo - Multiplying Large Integers\n",
    "\n",
    "The encryption and decryption steps of the RSA Algorithm require computing the products of very large integers.\n",
    "93,673 only requires 17 bits of storage, or less than 3 bytes.\n",
    "What about a number such as $26^{93673}$?\n",
    "The Python programming language will gladly attempt to compute it and succeeds on a modern laptop.\n",
    "\n",
    "```python\n",
    ">>> pow(26, 93673).bit_length()\n",
    "440305\n",
    "```\n",
    "\n",
    "That is a massive number over 134,000 digits long!\n",
    "Thankfully, this particular result is not needed, but the result modulus another number.\n",
    "For example, using a number easily computed by hand, what is $26^{307}\\pmod{47}$?\n",
    "Start by converting the exponent to binary.\n",
    "307 in base-2 is equal to 100110011.\n",
    "Exponentiation works in $\\mathbb{Z}_n$ because of the properties listed earlier.\n",
    "An exponent is simply repeated multiplication.\n",
    "If $b \\equiv a^x \\pmod{n}$ and $c\\equiv a^y\\pmod{n}$, then $bc \\equiv a^{x+y}\\pmod{n}$.\n",
    "$$\n",
    "\\begin{aligned}\n",
    "\t26^{307} &= 26^{2^0+2^1+2^4+2^5+2^8} \\\\\n",
    "\t\t\t &\\equiv 307^{2^0}+307^{2^1}+307^{2^4}+307^{2^5}+307^{2^8}\\pmod{47} \\\\\n",
    "\\end{aligned}\n",
    "$$\n",
    "Calculate $2^i$ where $i=0, 1, 4, 5, 8$.\n",
    "$$26^{2^1}=676\\equiv18 \\pmod{47}$$\n",
    "This can be squared to find $26^{2^2}\\pmod{47}$.\n",
    "$$\\begin{aligned}\n",
    "\t26^{2^2}&\\equiv (26^{2^1})^2\\pmod{47} \\\\\n",
    "\t\t\t&\\equiv (18)^2\\pmod{47} \\\\\n",
    "\t\t\t&\\equiv 324\\pmod{47} \\\\\n",
    "\t\t\t&\\equiv 42\\pmod{47} \\\\\n",
    "\\end{aligned}$$\n",
    "Use the fact that $(a^{2^n})^2\\equiv a^{2\\cdot2^n}\\equiv a^{2^{n+1}}\\pmod{n}$ \\parencite{abstract}.\n",
    "$$\\begin{aligned}\n",
    "\t26^{2^4}&\\equiv 14\\pmod{47} \\\\\n",
    "\t26^{2^5}&\\equiv 8\\pmod{47} \\\\\n",
    "\t26^{2^8}&\\equiv 2\\pmod{47} \\\\\n",
    "\t26^{307}&\\equiv 26*18*14*8*2 \\pmod{47}\\\\\n",
    "\t&\\equiv 104832 \\pmod{47}\\\\\n",
    "\t&\\equiv 22 \\pmod{47}\\\\\n",
    "\\end{aligned}$$\n",
    "\n",
    "Other than speed, this method saves memory as well since the largest number the computer must store is $2\\log_2{exp}$ bits.\n",
    "In general, an $n$-bit number multiplied by an $n$-bit number is $2n$-bits long.\n",
    "There are two approaches known as left-to-right (LR) and right-to-left (RL) depending on the direction that the bits of the exponent are scanned.\n",
    "The method above uses the RL method, but [LR is more common in hardware implementations](https://cetinkayakoc.net/docs/r02.pdf) of the calculation.\n",
    "\n",
    "## Euler’s theorem\n",
    "\n",
    "The RSA algorithm works because of Euler’s theorem.\n",
    "This is consistently the most hand-wavy aspect of most articles I read on this subject.\n",
    "It appears magical, but it's a beautiful result in mathematics if you dig into it a bit!\n",
    "\n",
    "### Theorem\n",
    "\n",
    "Let $a$ and $n$ be integers such that $n > 0$ and $gcd(a, n) = 1$.\n",
    "Then $a^{\\phi(n)} \\equiv 1 (\\pmod n)$.\n",
    "\n",
    "Recall that step 5 of the key selection requires finding\n",
    "\n",
    "$$td\\equiv 1\\pmod{\\phi(N)}.$$\n",
    "\n",
    "Using the definition of [modular congruence](https://blog.wificidr.net/posts/prime-numbers-the-extended-euclidean-algorithm-and-the-gcd/),\n",
    "\n",
    "$$td = 1 + k\\cdot \\phi(N)$$\n",
    "\n",
    "for some integer $k$.\n",
    "The message is encrypted by finding $a^t \\pmod{N}$ and decrypted by raising the encrypted message to the power of $d\\pmod{N}$.\n",
    "\n",
    "$$(a^t)^d \\pmod{N}$$\n",
    "\n",
    "This gives back $a$, the original message.\n",
    "\n",
    "$$\\begin{aligned}\n",
    "\t(a^t)^d &= a^{td} \\\\\n",
    "\t\t\t&= a^{1+k\\cdot\\phi(N)} \\\\\n",
    "\t\t\t&= a\\cdot \\left( a^{\\phi(N)} \\right)^k \\\\\n",
    "\t\t\t&\\equiv a\\cdot1^k \\\\\n",
    "\t\t\t&\\equiv a\\pmod{N} \\\\\n",
    "\\end{aligned}$$\n",
    "\n",
    "Euler's theorem may appear a bit magical at first.\n",
    "Consider the Caley table of integers $\\pmod{9}$.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e550e20c-ca6c-4dca-8f11-fac29a5b184e",
   "metadata": {},
   "source": [
    "![Caley table](/images/caley.png)\n",
    "\n",
    "Take note of the entries with a value of 1.\n",
    "These only occur when a number is coprime with 9.\n",
    "The number of 1 entries in the Caley table is the same value as $\\phi(n)$.\n",
    "For example, 11 is coprime with 9.\n",
    "\n",
    "$$\\begin{aligned}\n",
    "\t2 \\equiv 11 \\pmod{9} \\\\\n",
    "\t11^2 \\equiv 4 \\pmod{9} \\\\\n",
    "\t11^3 \\equiv 8 \\pmod{9} \\\\\n",
    "\t11^4 \\equiv 7 \\pmod{9} \\\\\n",
    "\t11^5 \\equiv 5 \\pmod{9} \\\\\n",
    "\t11^6 \\equiv 1 \\pmod{9} \\\\\n",
    "\\end{aligned}$$\n",
    "\n",
    "This process works the same even if you start with a value of 2.\n",
    "Increasing the power will always work towards a value of 1 as long as the GCD of $a$ and $n$ is 1.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "88fb48e4-a454-4477-9b18-154aaae2faa0",
   "metadata": {},
   "source": [
    "## Conclusion\n",
    "\n",
    "I hope my previous writing on this subject coupled with this explanation will give you a better idea of how RSA works under the hood.\n",
    "I think it rests on a beautiful set of results in mathematics, and the genius of the original creators is how they crafted these ideas into a useful protocol.\n",
    "Quantum computing has already made an impact on cryptography, but I trust this algorithm will be around for many more years as we transition to new, post-quantum technologies.\n"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.4"
  },
  "nikola": {
   "category": "",
   "date": "2023-12-19 18:26:05-06:00",
   "description": "",
   "link": "",
   "slug": "the-rsa-algorithm",
   "tags": "math,rsa",
   "title": "The RSA Algorithm",
   "type": "text"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
